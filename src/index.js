import React from "react";
import {render} from "react-dom";
import App from "./App.js";
import 'bootstrap/dist/css/bootstrap.min.css'
import '@fortawesome/fontawesome-free/css/svg-with-js.css';

const target = document.getElementById('root');

render(
    <App/>, target
);
