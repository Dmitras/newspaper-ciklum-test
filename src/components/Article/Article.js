import React, {Component, Suspense} from 'react';
import styled from "styled-components";
import {deleteNews, editNews} from '../../store/actions/index'
import {connect} from "react-redux";
import Preloader from "../UI/Preloader";

const EditArticle = React.lazy(() => import(`./EditArticle`));


const Image = styled('img')({
    margin: "auto",
    maxWidth: "100%",
    maxHeight: '30vh',
    overflow: 'hidden',
    objectFit: 'cover',
})

const Sfront = styled.div`
    background-image: ${props => (props.length >= 3) ? `url(${props.imageUrl}&height=${props.width * 100})` :
    (props.length === 2) ? `url(${props.imageUrl}&height=${570})` : `url(${props.imageUrl}&height=${1140})`};
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;


class Article extends Component {
    state = {
        editArticle: false,
    }

    onEditHandler = (title) => {
        this.setState({
                editArticle: true,
                inputText: title
            }
        )
    }

    onChangeHandler = (e) => {
        this.setState({
            inputText: e.target.value
        })
    }

    onCancelHandler = () => {
        this.setState({
            editArticle: false
        })
    }

    onSaveHandler = () => {
        const {editNews, article: {url}} = this.props
        const inputText = this.state.inputText
        editNews(url, inputText)
        this.setState({
            editArticle: false
        })
    }

    onDeleteHandler = (id) => {
        const {deleteNews} = this.props
        deleteNews(id)
    }

    render() {
        const {imageUrl, title, type, url, width} = this.props.article;
        const {length} = this.props;
        const editArticle = this.state.editArticle


        return (
            <div
                className="card"
            >
                <Suspense
                    fallback={
                        <Preloader
                            style={{width: '3rem', height: '3rem'}}
                            className='loading'>
                            Loading...
                        </Preloader>}>
                    <input type="checkbox" id={url} className="more"/>
                    {/*{*/}
                    {/*    (length > 2) ?*/}
                    {/*        <Image className="card-img-top" src={`${imageUrl}&height=${width * 100}`} alt="Card image cap"/>*/}
                    {/*        : (length === 2) ? <Image className="card-img-top" src={`${imageUrl}&width=${570}`} alt="Card image cap"/> :*/}
                    {/*        <Image className="card-img-top" src={`${imageUrl}&width=${1140}`} alt="Card image cap"/>*/}
                    {/*}*/}
                    <div className="content">
                        <div className={'card-cover'}>

                            <Sfront
                                className={'front'}
                                imageUrl={imageUrl}
                                width={width}
                                // length{length}
                            >
                                <div className={'inner'}>
                                    {
                                        (length >= 3) ?
                                            <h3 className="card-title">{title}</h3>
                                            : (length === 2) ?
                                            <h2 className="card-title">{title}</h2>
                                            :
                                            <h1 className="card-title">{title}</h1>
                                    }
                                    <div className="card-bottom">
                                        <label onClick={() => this.onEditHandler(title)} htmlFor={url}
                                               className="button"
                                               aria-hidden="true">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                 fill="currentColor"
                                                 className="bi bi-pen" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd"
                                                      d="M13.498.795l.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
                                            </svg>
                                        </label>
                                        <label onClick={() => this.onDeleteHandler(url)} className="button"
                                               data-dismiss="modal" aria-label="Close">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                 data-icon="times"
                                                 className="svg-inline--fa fa-times fa-w-11" role="img"
                                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                                <path fill="currentColor"
                                                      d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
                                            </svg>
                                        </label>
                                    </div>
                                </div>
                            </Sfront>

                            <div className="back">
                                <div className="inner">
                                    <EditArticle
                                        url={url}
                                        title={title}
                                        onChangeHandler={this.onChangeHandler}
                                        onCancelHandler={this.onCancelHandler}
                                        onSaveHandler={this.onSaveHandler}
                                        inputText={this.state.inputText}
                                    />
                                </div>
                            </div>

                        </div>
                    </div>
                </Suspense>

            </div>
        );
    }

}

const mapDispatchToProps = dispatch => {
    return {
        deleteNews: (id) => dispatch(deleteNews(id)),
        editNews: (id, title) => dispatch(editNews(id, title))
    }
}

export default connect(null, mapDispatchToProps)(Article);
