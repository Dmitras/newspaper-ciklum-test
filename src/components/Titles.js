import React from 'react';
import styled from "styled-components";

const Titles = ({rows}) => {

    const SThumb = styled.div`
    width: 100px;
    height: 100px;
    background-image: ${props => `url(${props.imageUrl}&height=${props.width * 100})`};
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
`;

    console.log(rows);

    return (
        rows ? (rows.map(row => (
            <div className={`list-group`}>

                {row.map(article =>
                    (

                        <a href={`${article.url}`}
                           className="list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-between">
                                <h5 className="mb-1">{article.title}</h5>
                                <small>{article.type}</small>
                            </div>
                            <p className="mb-1">{`Link to: ${article.url}`}</p>
                            <SThumb imageUrl={article.imageUrl} />
                        </a>
                    )
                )}
            </div>
        ))) : ''
    );
};

export default Titles;