import React from 'react';
import {Nav, Navbar, NavbarBrand, NavItem} from "reactstrap";
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <Navbar light expand='md'>
                <NavbarBrand href="#">
                    <NavLink className={'nav-link'} to="/">Dagbladet</NavLink>
                </NavbarBrand>
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink className={'nav-link'} to="/">News</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink className={'nav-link'} to="/titles">Titles</NavLink>
                    </NavItem>
                </Nav>
            </Navbar>
        </div>
    );
};

export default Header;
