import React from 'react';
import styled from 'styled-components';
import { Spinner } from "reactstrap";

const SLoading = styled(Spinner)`
  display: flex;
  margin: 1em auto;
  height: 100%;
  width: 100%;
`;

const Preloader = (props) => {
    return (
        <SLoading {...props} />
    );
};

export default Preloader;