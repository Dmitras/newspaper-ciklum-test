import React from 'react';
import Wrapper from "../../hoc/Wrapper";
import Header from '../../components/Header'
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Layout = ( props ) => {
    return (
        <Wrapper>
            <Header />
            <main>
                {props.children}
            </main>
            <ToastContainer autoClose={5000} />
        </Wrapper>
    );
};

export default Layout;
