import React, {Suspense} from 'react';
import Article from "./Article/Article";


const Row = ({articles, deletedArticles}) => {
    function renderArticles() {
        let articleLength;
        return articles
            .filter(article => {
                if (deletedArticles.length) {
                    for (let item of deletedArticles) {
                        articleLength = item.length
                        return item.url !== article.url
                    }
                } else {
                    return article
                }
            })
            .map(article =>
                (
                    <Article
                        key={article.url}
                        article={article}
                        length={articleLength ? articleLength : articles.length}
                    />
                )
            )
    }

    return (
        <div className={'row'}>
            <div className={`card-group`}>
                {renderArticles()}
            </div>
        </div>
    );
};

export default Row;
