import React from 'react';
import Row from "./Row";
import { connect } from "react-redux";

const News = (props) => {
    const {rows, deletedArticles} = props
    function renderRows(){
        return rows.map((articles, i) => (
            <Row key={i} articles={articles} deletedArticles={deletedArticles} />
        ))
    }

    return (
        <div className='container'>
            {renderRows()}
        </div>
    );
};

const mapStateToProps = ({ items }) => ({
    deletedArticles: items.deletedArticles,
})

export default connect(mapStateToProps, null)(News);
