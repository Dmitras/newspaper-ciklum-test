import { GET_NEWS, NEWS_LOADING, DELETE_NEWS, EDIT_NEWS, UNDO, WITHOUT_DELETED} from "../actions/types";

const initialState = {
    rows: [],
    deletedArticles: []
}

const deletedArticles = (data, url) => {
    let articles = [];

    data.rows.map(row => {
        return row.filter(article => {
            let length = row.length - 1
            if (article.url === url) {
                articles.push({url: article.url, length: length})
            }
        })
    })
    return [...data.deletedArticles, ...articles];
}

export default function (state = initialState, {type, payload}) {
    switch (type) {
        case GET_NEWS:
            return {
                ...state,
                rows: payload
            }

        case NEWS_LOADING:
            return {
                ...state,
                isLoading: payload
            }

        case DELETE_NEWS:
            return {
                ...state,
                rows: state.rows.map(row => {
                        return row
                    }
                ),
                deletedArticles: deletedArticles(state, payload)
            }

        case WITHOUT_DELETED:
            return {
                ...state,
                rows: state.rows.map(row => {
                        return row.filter(article => {
                            if (state.deletedArticles.length) {
                                for (let item of state.deletedArticles) {
                                    return item.url !== article.url
                                }
                            } else {
                                return article
                            }

                        })
                    }
                ),
                deletedArticles: []
            }


        case EDIT_NEWS:
            const updatedState = state.rows.map(row => {
                return row.map(article => {
                    if (article.url !== payload.id) {
                        return article
                    }
                    return {...article, title: payload.title}
                })
            })

            return {
                rows: updatedState,
                deletedArticles: []
            }

        case UNDO:
            return {
                ...state,
                rows: state.rows.map(row => {
                        return row
                    }
                ),
                deletedArticles: state.deletedArticles.filter(item => item.url !== payload)
            }

        default:
            return state
    }
}
