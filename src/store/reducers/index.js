import { combineReducers } from "redux";
import itemReducer from './newsReducer';
import isLoadingReducer from "./isLoadingReducer";

export default combineReducers({
    items: itemReducer,
    loading: isLoadingReducer
})

