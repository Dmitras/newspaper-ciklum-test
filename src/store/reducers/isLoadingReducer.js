import { NEWS_LOADING } from "../actions/types";

const initialState = {
    isLoading: false
}

export default function (state = initialState, { type, payload }) {
    switch (type) {
        case NEWS_LOADING:
            return {
                ...state,
                isLoading: payload
            }


        default:
            return {
                state,
            }
    }
}
