import React from 'react';
import { GET_NEWS, EDIT_NEWS, DELETE_NEWS, NEWS_LOADING, UNDO } from "./types";
import { toast } from "react-toastify";

const normalizeResponse = (data) => {
    let rows = []

    data.map(item => {
        for (let row of item) {
            rows.push(row.columns)
        }
    })
    return rows;
}

const Undo = ({onUndo, closeToast}) => {
    const handleClick = () => {
        onUndo();
        closeToast();
    };

    return (
        <div>
            <button onClick={handleClick} type="button" className="btn btn-success">UNDO</button>
        </div>
    );
};


export const getNews = () => {
    let isLoading = true;

    return async (dispatch) => {
        dispatch(setNewsLoading(isLoading));

        try {
            let response = await fetch('https://storage.googleapis.com/aller-structure-task/test_data.json')
            let news = await response.json()

            const data = normalizeResponse(news);
            dispatch(getNewsSuccess(data));
            isLoading = false;
            dispatch(setNewsLoading(isLoading));
        }
        catch (err) {
            dispatch(setNewsLoading(isLoading));
            console.log(err);
        }
        finally {
            isLoading = false
            dispatch(setNewsLoading(isLoading))
        }
    }
}

export const setNewsLoading = (data) => {
    return {
        type: NEWS_LOADING,
        payload: data
    }
}

export const getNewsSuccess = (data) => {
    return {
        type: GET_NEWS,
        payload: data
    }
}

export const deleteNews = (id) => {
    return (dispatch) => {
        dispatch({
            type: DELETE_NEWS,
            payload: id
        });
        toast(
            <Undo onUndo={() => dispatch({ payload: id, type: 'UNDO'})} />, {
                onClose: () => dispatch({ payload: id, type: 'WITHOUT_DELETED' })
            }
        );
    }
}

export const editNews = (id, title) => {
    return {
        type: EDIT_NEWS,
        payload: {id, title}
    }
}
