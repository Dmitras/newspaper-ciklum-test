export const GET_NEWS = 'GET_NEWS';
export const EDIT_NEWS = 'EDIT_NEWS';
export const DELETE_NEWS = 'DELETE_NEWS';
export const NEWS_LOADING = 'NEWS_LOADING';
export const UNDO = 'UNDO';
export const WITHOUT_DELETED = 'WITHOUT_DELETED';
