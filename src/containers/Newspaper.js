import React, {Component} from 'react';
import '../App.scss';
import {
    Switch,
    Route,
} from "react-router-dom";
import News from "../components/News";
import Titles from "../components/Titles";
import {connect} from 'react-redux';
import {getNews} from "../store/actions";


class Newspaper extends Component {
    state = {
        isLoading: false
    }

    componentDidMount() {
        this.props.getNews()
    }


    render() {
        const {rows, isLoading} = this.props;


        return (
            <div className="container">
                {!isLoading ? (
                    <Switch>
                        <Route
                            exact path='/'
                            render={(props) =>
                                <News
                                    {...props}
                                    rows={rows}
                                />
                            }
                        />
                        <Route
                            exact path='/titles'
                            render={(props) =>
                                <Titles
                                    {...props}
                                    rows={rows}
                                />
                            }
                        />
                    </Switch>
                ) : 'Loading...'}
            </div>
        )
    }
}

const mapStateToProps = ({items, loading}) => ({
    rows: items.rows,
    isLoading: loading.isLoading,
    deletedArticles: items.deletedArticles,
})


export default connect(mapStateToProps, {getNews})(Newspaper);
