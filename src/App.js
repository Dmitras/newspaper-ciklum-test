import React, {Component} from 'react';
import {Provider} from 'react-redux';
import store from './store/store';
import history from "./helpers/history";
import Layout from './components/Layout/Layout';
import Newspaper from './containers/Newspaper';
import {
    BrowserRouter as Router,
} from "react-router-dom";

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <div className={'App'}>
                    <Router history={history}>
                        <Layout>
                            <Newspaper/>
                        </Layout>
                    </Router>
                </div>
            </Provider>
        );
    }
}

export default App;
